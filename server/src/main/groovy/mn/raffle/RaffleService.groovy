package mn.raffle

import com.google.api.services.gmail.GmailScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GmailService
import com.puravida.groogle.GmailServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.SheetService
import com.puravida.groogle.SheetServiceBuilder

import javax.annotation.PostConstruct
import javax.inject.Singleton

@Singleton
class RaffleService {

    String[] participantsRange = ['C2','E99']
    String[] pricesRange = ['A2','B99']

    SheetService sheetService
    GmailService gmailService

    @PostConstruct
    void init() {
        Groogle groogle = GroogleBuilder.build {
            withServiceCredentials {
                withScopes SheetsScopes.SPREADSHEETS, GmailScopes.MAIL_GOOGLE_COM
                //usingCredentials "client_secret.json"
            }
            service(SheetServiceBuilder.build(), SheetService)
            service(GmailServiceBuilder.build(), GmailService)
        }

        sheetService = groogle.service(SheetService) as SheetService
        gmailService = groogle.service(GmailService) as GmailService
    }

    List<Participant> loadParticipants(String sheetId, String tabId){
        List people = []

        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                writeRange participantsRange[0], participantsRange[1], {
                    get().eachWithIndex{ def entry, int i ->
                        if( entry[0] && !entry[1])
                            people.add new Participant(name:entry[0], email: entry[2])
                    }
                }
            }
        }
        people.sort { Math.random() }
    }

    List<Prize> loadPrizes(String sheetId, String tabId){
        List prizes = []
        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                writeRange pricesRange[0],pricesRange[1], {
                    get().eachWithIndex{ def entry, int i ->
                        if( entry[0] ) {
                            prizes.add new Prize(name: entry[0], quantity: (entry[1] ?: 0) as int)
                        }
                    }
                }
            }
        }
        prizes
    }

    Boolean notPresent(String sheetId, String tabId, String name){
        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                List<List<String>> range
                range = writeRange(participantsRange[0],participantsRange[1]).get()
                List<String> rwinner = range.find{ it[0] && it[0] == name}
                rwinner[1] = "NOT PRESSENT"
                writeRange(participantsRange[0],participantsRange[1]).set(range)
            }
        }
        true

    }

    Boolean winner(String sheetId, String tabId, String prize, String name){
        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                List<List<String>> range

                range = writeRange(participantsRange[0],participantsRange[1]).get()
                List<String> rwinner = range.find{ it[0] && it[0] == name}
                rwinner[1] = prize
                writeRange(participantsRange[0],participantsRange[1]).set(range)

                range = writeRange(pricesRange[0],pricesRange[1]).get()
                List<String> rprize = range.find{ it[0] && it[0] == prize}
                rprize[1]= (rprize[1] as int) -1
                writeRange(pricesRange[0],pricesRange[1]).set(range)
            }
        }
        true
    }

    Participant raffle(String sheetId, String tabId, String prize){
        List<Participant> participants = loadParticipants(sheetId, tabId)
        Participant winner = participants[ new Random().nextInt(participants.size())]
        sheetService.withSpreadSheet sheetId, {
            withSheet tabId, {
                List<List<String>> range

                range = writeRange(participantsRange[0],participantsRange[1]).get()
                List<String> rwinner = range.find{ it[0] && it[0] == winner.name}
                rwinner[1] = prize
                writeRange(participantsRange[0],participantsRange[1]).set(range)

                range = writeRange(pricesRange[0],pricesRange[1]).get()
                List<String> rprize = range.find{ it[0] && it[0] == prize}
                rprize[1]=rprize[1]-1
                writeRange(pricesRange[0],pricesRange[1]).set(range)
            }
        }
        winner
    }
}
