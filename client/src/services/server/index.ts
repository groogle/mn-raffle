import axios from 'axios'
import http from '@/services/http'

export default new class {

    async loadUser(){
        const res = await http.get(process.env.VUE_APP_API_URL + `api/v1/user`);
        return res.data;
    }

    async loadPrizes( groogleData: any){
        const res = await http.post(process.env.VUE_APP_API_URL + `api/v1/raffle/prizes`, groogleData);
        return res.data;
    }

    async loadParticipants(groogleData: any){
        const res = await http.post(process.env.VUE_APP_API_URL + `api/v1/raffle/participants`, groogleData);
        return res.data;
    }

    async winner(groogleData: any, prize: string, winner: string){
        const data = groogleData
        data.prize = prize
        data.winner = winner
        const res = await http.post(process.env.VUE_APP_API_URL + `api/v1/raffle/winner`, data);
        return res.data;
    }

    async notPresent(groogleData: any, winner: string){
        const data = groogleData
        data.winner = winner
        const res = await http.post(process.env.VUE_APP_API_URL + `api/v1/raffle/notpressent`, data);
        return res.data;
    }
}
