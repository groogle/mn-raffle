
import mock from "./data/index.json"
import store from '@/store';

const user = mock.user
const prizes = mock.prizes
console.log("pizes "+prizes)
const vparticipants = mock.participants

const mockFetchData = (mockData: any, time = 0) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            store.commit('loader/FINISH_LOADING');
            resolve(mockData)
        }, time)
    })
}

export default new class {
    loadUser () {
        store.commit('loader/START_LOADING');
        return mockFetchData(user, 1000) // wait 1s before returning posts
    }
    loadPrizes( groogleData: any){
        store.commit('loader/START_LOADING');
        return mockFetchData(prizes, 1000) // wait 1s before returning posts
    }
    loadParticipants(groogleData: any){
        store.commit('loader/START_LOADING');
        return mockFetchData(vparticipants, 1000) // wait 1s before returning posts
    }
    winner(groogleData: any, prize: string, name: string){
        store.commit('loader/START_LOADING');
        const found = prizes.find( element => element.name === prize )
        if( found ) {
            found.quantity--
        }
        const winner = vparticipants.findIndex( element => element.name == name)
        vparticipants.splice(winner,1)
        return mockFetchData(true, 1000)
    }
    notPresent(groogleData: any, name: string){
        store.commit('loader/START_LOADING');
        const winner = vparticipants.findIndex( element => element.name == name)
        vparticipants.splice(winner,1)
        return mockFetchData(true, 1000)
    }
}
