import Vue from 'vue'
import Vuex from 'vuex'
import { GetterTree, MutationTree, ActionTree } from "vuex"
import api from '@services/index'
import loader from "@/store/loading";

Vue.use(Vuex)

class State {

    busy = false;

    user = {
        name:'',
        email:''
    };

    googleForm = {
        sheetId:'',
        tabId:''
    };

    participants = [];
    prizes =  [];

    winner =  '';
    prize =  '';
}

export default new Vuex.Store<State>({

    state: new State(),

    mutations: {
        logged(state, user){
            state.user = user;
        },
        prizes(state, prizes){
            state.prizes = prizes;
        },
        participants(state, participants){
            state.participants = participants;
        },
        prize( state, prize){
            state.prize = prize
        },
        winner( state, winner){
            state.winner = winner
        },
        rejectWinner( state ) {
            state.participants.splice(
                state.participants.findIndex( (element)=> element['name']===state.winner), 1)
        }
    },

    actions: {
        fetchUser ( context ) {
            return api
                .loadUser()
                .then( (user: any) => context.commit('logged', user))
        },
        fetchPrizes( context ){
            return api
                .loadPrizes( context.state.googleForm )
                .then((prizes: any) => context.commit('prizes', prizes))
        },
        fetchParticipants( context ){
            return api
                .loadParticipants(context.state.googleForm)
                .then((participants: any) => context.commit('participants', participants))
        },
        raffle( context, prize ){
            const arr = context.state.participants
            const winner = arr[Math.floor(Math.random() * arr.length)]
            context.commit('prize', prize)
            context.commit('winner', winner['name'])
        },
        acceptWinner( context){
            return api
                .winner( context.state.googleForm, context.state.prize, context.state.winner)
                .then( () => context.dispatch('fetchPrizes') )
                .then( () => context.dispatch('fetchParticipants') )
        },
        notPresent(context){
            return api
                .notPresent( context.state.googleForm, context.state.winner)
                .then( () => context.dispatch('fetchParticipants') )
        }
    },

    modules:{
        loader
    }
})
